// Get the current URL and its origin
var url = window.location.href;
var origin = window.location.origin;

// Check if the URL is loaded through a web proxy
if (origin !== 'https://now.gg/es/apps/roblox-corporation/5349/roblox.html') {
  // The website is not loaded through a web proxy, so show a message
  document.body.innerHTML = 'You must load this page through a web proxy. Made by PDFz (and chatgpt lmao)';
  return;
}

// The website is loaded through a web proxy, so allow it to load
document.body.style.display = 'block';